from app import database


class User(database.Model):
    __table_args__ = {
        'mysql_engine': 'InnoDB',
        'mysql_charset': 'utf8'
    }
    __tablename__ = 'users'
    uid = database.Column(database.Integer, primary_key=True, unique=True, autoincrement=True)
    privilege = database.Column(database.Text)  # supervisor/propertyAdmin/propertyDeputy/owner
    password = database.Column(database.Text)
    status = database.Column(database.Integer)
    name = database.Column(database.Text)
    tid = database.Column(database.Text)
    email = database.Column(database.Text)
    telephone = database.Column(database.Text)
    area = database.Column(database.Text)
    room = database.Column(database.Text)
    peopleCount = database.Column(database.Integer)
    parkingLot = database.Column(database.Text)

    def __repr__(self):
        return '<Database Object: User %r>' % self.name

    def toDict(self):
        return {
            "uid": self.uid,
            "privilege": self.privilege,  # supervisor/propertyAdmin/propertyDeputy/owner
            "password": self.password,
            "status": self.status,
            "name": self.name,
            "tid": self.tid,
            "email": self.email,
            "telephone": self.telephone,
            "area": self.area,
            "room": self.room,
            "peopleCount": self.peopleCount,
            "parkingLot": self.parkingLot
        }


class Fee(database.Model):
    __table_args__ = {
        'mysql_engine': 'InnoDB',
        'mysql_charset': 'utf8'
    }
    __tablename__ = 'fee'
    id = database.Column(database.Integer, primary_key=True, autoincrement=True)
    type = database.Column(database.Text)  # 水费、电费、物业管理费、维修费、其他费用
    description = database.Column(database.Text)
    amount = database.Column(database.Float)
    date = database.Column(database.Date)
    status = database.Column(database.Text)
    uid = database.Column(database.Integer, database.ForeignKey("users.uid", ondelete="CASCADE"))
    superUid = database.Column(database.Integer, database.ForeignKey("users.uid", ondelete="CASCADE"))

    def __init__(self, type, description, amount, date, status, uid, superUid):
        self.type = type
        self.description = description
        self.amount = amount
        self.date = date
        self.status = status
        self.uid = uid
        self.superUid = superUid

    def toDict(self):
        return {
            "id": self.id,
            "type": self.type,
            "description": self.description,
            "amount": self.amount,
            "date": str(self.date),
            "status": self.status,
            "uid": self.uid,
            "superUid": self.superUid
        }

    def __repr__(self):
        return '<Database Object: Fee %r>' % self.id


class WaterElectricity(database.Model):
    __table_args__ = {
        'mysql_engine': 'InnoDB',
        'mysql_charset': 'utf8'
    }
    __tablename__ = 'life'
    id = database.Column(database.Integer, primary_key=True, autoincrement=True)
    water = database.Column(database.Float)
    electricity = database.Column(database.Float)
    waterPrice = database.Column(database.Float)
    electricityPrice = database.Column(database.Float)
    date = database.Column(database.Date)
    status = database.Column(database.Text)
    uid = database.Column(database.Integer, database.ForeignKey("users.uid", ondelete="CASCADE"))
    superUid = database.Column(database.Integer, database.ForeignKey("users.uid", ondelete="CASCADE"))

    def __init__(self, water, electricity, waterPrice, electricityPrice, date, status, uid, superUid):
        self.water = water
        self.electricity = electricity
        self.waterPrice = waterPrice
        self.electricityPrice = electricityPrice
        self.date = date
        self.status = status
        self.uid = uid
        self.superUid = superUid

    def toDict(self):
        return {
            "id": self.id,
            "water": self.water,
            "electricity": self.electricity,
            "waterPrice": self.waterPrice,
            "electricityPrice": self.electricityPrice,
            "date": str(self.date),
            "status": self.status,
            "uid": self.uid,
            "superUid": self.superUid
        }

    def __repr__(self):
        return '<Database Object: WaterElectricity %r>' % self.id


class Event(database.Model):
    __table_args__ = {
        'mysql_engine': 'InnoDB',
        'mysql_charset': 'utf8'
    }
    __tablename__ = 'event'
    id = database.Column(database.Integer, primary_key=True, autoincrement=True)
    description = database.Column(database.Text)
    status = database.Column(database.Text)
    uid = database.Column(database.Integer, database.ForeignKey("users.uid", ondelete="CASCADE"))
    data = database.Column(database.Text)

    def __init__(self, description, status, uid, data):
        self.description = description
        self.status = status
        self.uid = uid
        self.data = data

    def toDict(self):
        return {
            "id": self.id,
            "description": self.description,
            "status": self.status,
            "uid": self.uid,
            "data": self.data
        }

    def __repr__(self):
        return '<Database Object: Event %r>' % self.id
