from app import app
from .routes.root import root
from .routes.admin import admin

app.register_blueprint(root, url_prefix='/api')
app.register_blueprint(admin, url_prefix='/api/admin')

