import json
from flask import Blueprint, request, session
from sqlalchemy import or_, func, and_
from app import database
from ..models import User, Fee, WaterElectricity, Event

admin = Blueprint("admin", __name__)


# Redstone-release
@admin.route("/", methods=["GET"])
def root():
    if session.get("privilege") not in ["supervisor", "propertyAdmin", "propertyDeputy"]:
        return json.dumps({
            "status": 1,
            "msg": "at judgement privilege: Forbidden."
        })


# Redstone-release
@admin.route("/user", methods=["GET"])
def user_get():
    if session.get("privilege") not in ["supervisor", "propertyAdmin"]:
        return json.dumps({
            "status": 1,
            "msg": "at judgement privilege: Forbidden."
        })

    start = request.args.get("start", 0, int)
    count = request.args.get("count", 10, int)

    try:
        total = database.session.query(func.count(User.uid)).scalar()
        rows = database.session.query(User).limit(count).offset(start).all()
    except:
        return json.dumps({
            "status": 1,
            "msg": "at query users info: SQL internal."
        })

    data = [{
        "id": row.uid,
        # Carbon Design Svelte DataTable needs an id, reference to
        # https://github.com/carbon-design-system/carbon-components-svelte/issues/596
        "uid": row.uid,
        "name": row.name,
        "primaryKey": row.telephone if row.email is None else row.email,
        "privilege": row.privilege,
        "status": "启用" if row.status == 0 else "停用"
    } for row in rows]
    return json.dumps({
        "status": 0,
        "data": data,
        "total": total
    })


# Redstone-release
@admin.route("/user/<int:uid>", methods=["GET"])
def user_id_get(uid):
    if session.get("privilege") not in ["supervisor", "propertyAdmin"]:
        return json.dumps({
            "status": 1,
            "msg": "at judgement privilege: Forbidden."
        })

    try:
        rows = database.session.query(User).filter(User.uid == uid).all()
    except:
        return json.dumps({
            "status": 1,
            "msg": "at query users info: SQL internal."
        })

    if len(rows) != 1:
        return json.dumps({
            "status": 1,
            "msg": "at judgement userinfo length: length is not 1."
        })

    return json.dumps({
        "status": 0,
        "data": rows[0].toDict()
    })


# Redstone-release
@admin.route("/user/<int:uid>", methods=["POST"])
def user_id_post(uid):
    if not session.get("isLogin"):
        return json.dumps({
            "status": 1,
            "msg": "at judgement isLogin: Not login yet."
        })
    try:
        data = json.loads(request.get_data(as_text=True))
    except:
        return json.dumps({
            "status": 1,
            "msg": "at json parse: JSON parse error."
        })

    # admin can't update upwards
    if (session.get("privilege") == "supervisor" or session.get("privilege") == "propertyAdmin") \
            and not (data["privilege"] == "supervisor" and session.get("privilege") == "propertyAdmin"):
        try:
            database.session.query(User).filter(User.uid == data["uid"]).update({
                User.password: data["password"],
                User.name: data["name"],
                User.tid: data["tid"],
                User.status: data["status"],
                User.privilege: data["privilege"],
                User.email: data["email"],
                User.telephone: data["telephone"],
                User.area: data["area"],
                User.room: data["room"],
                User.peopleCount: data["peopleCount"],
                User.parkingLot: data["parkingLot"]
            })
            database.session.commit()
        except:
            return json.dumps({
                "status": 1,
                "msg": "at query users info: SQL internal."
            })
    else:
        return json.dumps({
            "status": 1,
            "msg": "at judgement privilege: Insufficient privilege update."
        })
    return json.dumps({
        "status": 0,
        "msg": "userinfo update successfully."
    })


# Redstone-release
@admin.route("/user/add", methods=["POST"])
def user_add():
    if not session.get("isLogin"):
        return json.dumps({
            "status": 1,
            "msg": "at judgement isLogin: Not login yet."
        })
    try:
        data = json.loads(request.get_data(as_text=True))
    except:
        return json.dumps({
            "status": 1,
            "msg": "at json parse: JSON parse error."
        })

    password = data.get("password", None)
    name = data.get("name", None)
    privilege = data.get("privilege", None)
    tid = data.get("tid", None)
    status = data.get("status", None)
    email = data.get("email", None)
    telephone = data.get("telephone", None)
    area = data.get("area", None)
    room = data.get("room", None)
    peopleCount = data.get("peopleCount", None)
    parkingLot = data.get("parkingLot", None)

    if (privilege == "supervisor" and session.get("privilege", None) != "supervisor") \
            or (privilege in ["supervisor", "propertyAdmin"] and session.get("privilege", None) == "propertyAdmin"):
        return json.dumps({
            "status": 1,
            "msg": "at judgement privilege: Insufficient privilege update."
        })

    if password is None or (
            email is None and telephone is None) or area is None or room is None or name is None or tid is None:
        return json.dumps({
            "status": 1,
            "msg": "at judgement not null: Insufficient data."
        })

    if len(database.session.query(User).filter(or_(
            and_(User.telephone == telephone, telephone is not None),
            and_(User.email == email, email is not None),
            User.tid == tid
    )).all()) != 0:
        return json.dumps({
            "status": 1,
            "msg": "at judgement duplication: Duplicated data."
        })

    try:
        database.session.add(User(
            privilege=privilege,
            password=password,
            name=name,
            status=status,
            tid=tid,
            email=email,
            telephone=telephone,
            area=area,
            room=room,
            peopleCount=peopleCount,
            parkingLot=parkingLot
        ))
        database.session.commit()
    except:
        return json.dumps({
            "status": 1,
            "msg": "at insertion: SQL internal."
        })

    return json.dumps({
        "status": 0,
        "msg": "User add successfully."
    })


# Redstone-release
@admin.route("/user/delete", methods=["POST"])
def user_remove():
    if not session.get("isLogin"):
        return json.dumps({
            "status": 1,
            "msg": "at judgement isLogin: Not login yet."
        })
    try:
        data = json.loads(request.get_data(as_text=True))
    except:
        return json.dumps({
            "status": 1,
            "msg": "at json parse: JSON parse error."
        })

    uids = data.get("uids", None)

    for uid in uids:
        if uid == 0:
            return json.dumps({
                "status": 1,
                "msg": "at remove: Could not remove supervisor."
            })

        privilege = database.session.query(User.privilege).filter(User.uid == uid).all()

        privilege = privilege[0].privilege
        selfPrivilege = session.get("privilege", None)
        if selfPrivilege not in ["supervisor", "propertyAdmin"] or selfPrivilege == privilege:
            return json.dumps({
                "status": 1,
                "msg": "at judgement privilege: Insufficient privilege update."
            })
        try:
            database.session.query(User).filter(User.uid == uid).delete()
            database.session.commit()
        except:
            return json.dumps({
                "status": 1,
                "msg": "at query users info: SQL internal."
            })
    return json.dumps({
        "status": 0,
        "msg": "remove userinfo successfully."
    })


# Redstone-release
@admin.route("/user/enable", methods=["POST"])
def user_enable():
    if not session.get("isLogin"):
        return json.dumps({
            "status": 1,
            "msg": "at judgement isLogin: Not login yet."
        })
    try:
        data = json.loads(request.get_data(as_text=True))
    except:
        return json.dumps({
            "status": 1,
            "msg": "at json parse: JSON parse error."
        })

    uids = data.get("uids", None)

    for uid in uids:
        if uid == 0:
            return json.dumps({
                "status": 1,
                "msg": "at remove: Could not operate supervisor."
            })

        privilege = database.session.query(User.privilege).filter(User.uid == uid).all()
        if len(privilege) != 1:
            return json.dumps({
                "status": 1,
                "msg": "at judgement userinfo length: length is not 1."
            })

        privilege = privilege[0].privilege
        selfPrivilege = session.get("privilege", None)
        if selfPrivilege not in ["supervisor", "propertyAdmin"] or selfPrivilege == privilege:
            return json.dumps({
                "status": 1,
                "msg": "at judgement privilege: Insufficient privilege update."
            })
        try:
            database.session.query(User).filter(User.uid == uid).update({
                User.status: 0
            })
            database.session.commit()
        except:
            return json.dumps({
                "status": 1,
                "msg": "at query users info: SQL internal."
            })
    return json.dumps({
        "status": 0,
        "msg": "enable user successfully."
    })


# Redstone-release
@admin.route("/user/disable", methods=["POST"])
def user_disable():
    if not session.get("isLogin"):
        return json.dumps({
            "status": 1,
            "msg": "at judgement isLogin: Not login yet."
        })
    try:
        data = json.loads(request.get_data(as_text=True))
    except:
        return json.dumps({
            "status": 1,
            "msg": "at json parse: JSON parse error."
        })

    uids = data.get("uids", None)

    for uid in uids:
        if uid == 0:
            return json.dumps({
                "status": 1,
                "msg": "at remove: Could not operate supervisor."
            })

        privilege = database.session.query(User.privilege).filter(User.uid == uid).all()
        if len(privilege) != 1:
            return json.dumps({
                "status": 1,
                "msg": "at judgement userinfo length: length is not 1."
            })

        privilege = privilege[0].privilege
        selfPrivilege = session.get("privilege", None)
        if selfPrivilege not in ["supervisor", "propertyAdmin"] or selfPrivilege == privilege:
            return json.dumps({
                "status": 1,
                "msg": "at judgement privilege: Insufficient privilege update."
            })
        try:
            database.session.query(User).filter(User.uid == uid).update({
                User.status: 1
            })
            database.session.commit()
        except:
            return json.dumps({
                "status": 1,
                "msg": "at query users info: SQL internal."
            })
    return json.dumps({
        "status": 0,
        "msg": "disabled user successfully."
    })


# Redstone-release
@admin.route("/fee", methods=["GET"])
def fee_get():
    if session.get("privilege") not in ["supervisor", "propertyAdmin", "propertyDeputy"]:
        return json.dumps({
            "status": 1,
            "msg": "at judgement privilege: Forbidden."
        })

    start = request.args.get("start", 0, int)
    count = request.args.get("count", 10, int)

    selfUid = session.get("uid", None)

    if session.get("privilege") == "propertyDeputy":
        try:
            total = database.session.query(func.count(Fee.id)).filter(Fee.superUid == selfUid).scalar()
            rows = database.session.query(Fee, User.email, User.telephone).join(User, User.uid == Fee.uid).filter(
                Fee.superUid == selfUid).limit(count).offset(start).all()
        except:
            return json.dumps({
                "status": 1,
                "msg": "at query users info: SQL internal."
            })
    else:
        try:
            total = database.session.query(func.count(Fee.id)).scalar()
            rows = database.session.query(Fee, User.email, User.telephone).join(User, User.uid == Fee.uid).limit(
                count).offset(start).all()
        except:
            return json.dumps({
                "status": 1,
                "msg": "at query users info: SQL internal."
            })

    data = [{
        "id": row[0].id,
        # Carbon Design Svelte DataTable needs an id, reference to https://github.com/carbon-design-system/carbon-components-svelte/issues/596
        "type": row[0].type,
        "amount": row[0].amount,
        "uid": row[0].uid,
        "date": str(row[0].date),
        "status": row[0].status,
        "primaryKey": row[1] if row[1] is not None else row[2]
    } for row in rows]
    return json.dumps({
        "status": 0,
        "data": data,
        "total": total
    })


# Redstone-release
@admin.route("/fee/add", methods=["POST"])
def fee_add():
    if not session.get("isLogin"):
        return json.dumps({
            "status": 1,
            "msg": "at judgement isLogin: Not login yet."
        })
    try:
        data = json.loads(request.get_data(as_text=True))
    except:
        return json.dumps({
            "status": 1,
            "msg": "at json parse: JSON parse error."
        })

    type = data.get("type", None)
    description = data.get("description", None)
    amount = data.get("amount", None)
    date = data.get("date", None)
    status = data.get("status", None)
    uid = data.get("uid", None)
    superUid = data.get("superUid", None)
    privilege = session.get("privilege", None)

    # None others can issue a fee to supervisor
    if privilege not in ["supervisor", "propertyAdmin", "propertyDeputy"] \
            or (uid == 0 and session.get("uid") != 0):
        return json.dumps({
            "status": 1,
            "msg": "at judgement privilege: Insufficient privilege update."
        })

    if type is None or amount is None or date is None or status is None or uid is None or superUid is None:
        return json.dumps({
            "status": 1,
            "msg": "at judgement not null: Insufficient data."
        })

    try:
        database.session.add(Fee(
            type=type,
            description=description,
            amount=amount,
            date=date,
            status=status,
            uid=uid,
            superUid=superUid
        ))
        database.session.commit()
    except:
        return json.dumps({
            "status": 1,
            "msg": "at insertion: SQL internal."
        })

    return json.dumps({
        "status": 0,
        "msg": "Fee add successfully."
    })


# Redstone-release
@admin.route("/fee/<int:feeId>", methods=["GET"])
def fee_id_get(feeId):
    if session.get("privilege", None) not in ["supervisor", "propertyAdmin", "propertyDeputy"]:
        return json.dumps({
            "status": 1,
            "msg": "at judgement privilege: Forbidden."
        })

    try:
        rows = database.session.query(Fee).filter(Fee.id == feeId).all()
    except:
        return json.dumps({
            "status": 1,
            "msg": "at query fee: SQL internal."
        })

    if len(rows) != 1:
        return json.dumps({
            "status": 1,
            "msg": "at judgement fee log length: length is not 1."
        })

    if session.get("privilege", None) == "propertyDeputy" and rows[0].superUid != session.get("uid", None):
        return json.dumps({
            "status": 1,
            "msg": "at judgement privilege: Forbidden."
        })

    return json.dumps({
        "status": 0,
        "data": rows[0].toDict()
    })


# Redstone-release
@admin.route("/fee/<int:feeId>", methods=["POST"])
def fee_id_post(feeId):
    if not session.get("isLogin"):
        return json.dumps({
            "status": 1,
            "msg": "at judgement isLogin: Not login yet."
        })
    try:
        data = json.loads(request.get_data(as_text=True))
    except:
        return json.dumps({
            "status": 1,
            "msg": "at json parse: JSON parse error."
        })

    if session.get("privilege") == "supervisor" or session.get("privilege") == "propertyAdmin":
        try:
            database.session.query(Fee).filter(Fee.id == data["id"]).update({
                Fee.type: data["type"],
                Fee.description: data["description"],
                Fee.amount: data["amount"],
                Fee.date: data["date"],
                Fee.status: data["status"],
                Fee.uid: data["uid"],
                Fee.superUid: data["superUid"] if (session.get("privilege") == "supervisor") else session.get("uid")
            })
            database.session.commit()
        except:
            return json.dumps({
                "status": 1,
                "msg": "at query users info: SQL internal."
            })
    elif session.get("privilege") == "propertyDeputy":
        try:
            rows = database.session.query(Fee.id).filter(Fee.superUid == session.get("uid")).all()
            if len(rows) < 1:
                return json.dumps({
                    "status": 1,
                    "msg": "at judgement privilege: Insufficient privilege update."
                })
            else:
                try:
                    database.session.query(Fee).filter(Fee.id == data["id"]).update({
                        Fee.type: data["type"],
                        Fee.description: data["description"],
                        Fee.amount: data["amount"],
                        Fee.date: data["date"],
                        Fee.status: data["status"],
                        Fee.uid: data["uid"],
                        Fee.superUid: data["superUid"] if (session.get("privilege") == "supervisor") else session.get(
                            "uid")
                    })
                    database.session.commit()
                except:
                    raise
        except:
            return json.dumps({
                "status": 1,
                "msg": "at query fee: SQL internal."
            })

    return json.dumps({
        "status": 0,
        "msg": "fee update successfully."
    })


# Redstone-release
@admin.route("/fee/markAsPaid", methods=["POST"])
def fee_mark_paid():
    if not session.get("isLogin"):
        return json.dumps({
            "status": 1,
            "msg": "at judgement isLogin: Not login yet."
        })
    try:
        data = json.loads(request.get_data(as_text=True))
    except:
        return json.dumps({
            "status": 1,
            "msg": "at json parse: JSON parse error."
        })

    feeIds = data.get("feeIds", None)

    for feeId in feeIds:
        selfPrivilege = session.get("privilege", None)
        if selfPrivilege not in ["supervisor", "propertyAdmin", "propertyDeputy"]:
            return json.dumps({
                "status": 1,
                "msg": "at judgement privilege: Insufficient privilege update."
            })

        if selfPrivilege in ["supervisor", "propertyAdmin"]:
            try:
                if len(database.session.query(Fee).filter(and_(Fee.id == feeId,
                                                               Fee.status == "canceled")).all()) > 0 and selfPrivilege == "propertyAdmin":
                    return json.dumps({
                        "status": 1,
                        "msg": "at check isCanceledFee: the fee has been canceled, privilege insufficient."
                    })
                database.session.query(Fee).filter(Fee.id == feeId).update({
                    Fee.status: "paid"
                })
                database.session.commit()
            except:
                return json.dumps({
                    "status": 1,
                    "msg": "at query fee: SQL internal."
                })
        else:
            try:
                if len(database.session.query(Fee).filter(and_(Fee.id == feeId, Fee.status == "canceled")).all()) > 0:
                    return json.dumps({
                        "status": 1,
                        "msg": "at check isCanceledFee: the fee has been canceled, privilege insufficient."
                    })
                if len(database.session.query(Fee).filter(
                        and_(Fee.id == feeId, session.get("uid") == Fee.superUid)).all()) < 1:
                    return json.dumps({
                        "status": 1,
                        "msg": "at query fee: No log found."
                    })
                database.session.query(Fee).filter(and_(Fee.id == feeId, session.get("uid") == Fee.superUid)).update({
                    Fee.status: "paid"
                })
                database.session.commit()
            except:
                return json.dumps({
                    "status": 1,
                    "msg": "at query fee: SQL internal."
                })
    return json.dumps({
        "status": 0,
        "msg": "marked fee log as paid successfully."
    })


# Redstone-release
@admin.route("/fee/markAsCanceled", methods=["POST"])
def fee_mark_canceled():
    if not session.get("isLogin"):
        return json.dumps({
            "status": 1,
            "msg": "at judgement isLogin: Not login yet."
        })
    try:
        data = json.loads(request.get_data(as_text=True))
    except:
        return json.dumps({
            "status": 1,
            "msg": "at json parse: JSON parse error."
        })

    feeIds = data.get("feeIds", None)

    for feeId in feeIds:
        selfPrivilege = session.get("privilege", None)
        if selfPrivilege not in ["supervisor", "propertyAdmin", "propertyDeputy"]:
            return json.dumps({
                "status": 1,
                "msg": "at judgement privilege: Insufficient privilege update."
            })

        if selfPrivilege in ["supervisor", "propertyAdmin"]:
            try:
                if len(database.session.query(Fee).filter(
                        and_(Fee.id == feeId, Fee.status == "paid")).all()) > 0 and selfPrivilege == "propertyAdmin":
                    return json.dumps({
                        "status": 1,
                        "msg": "at check isPaidFee: the fee has been paid, privilege insufficient."
                    })
                database.session.query(Fee).filter(Fee.id == feeId).update({
                    Fee.status: "canceled"
                })
                database.session.commit()
            except:
                return json.dumps({
                    "status": 1,
                    "msg": "at query fee: SQL internal."
                })
        else:
            try:
                if len(database.session.query(Fee).filter(and_(Fee.id == feeId, Fee.status == "paid")).all()) > 0:
                    return json.dumps({
                        "status": 1,
                        "msg": "at check isCanceledFee: the fee has been paid, privilege insufficient."
                    })
                if len(database.session.query(Fee).filter(
                        and_(Fee.id == feeId, session.get("uid") == Fee.superUid)).all()) < 1:
                    return json.dumps({
                        "status": 1,
                        "msg": "at query fee: No log found."
                    })
                database.session.query(Fee).filter(and_(Fee.id == feeId, session.get("uid") == Fee.superUid)).update({
                    Fee.status: "canceled"
                })
                database.session.commit()
            except:
                return json.dumps({
                    "status": 1,
                    "msg": "at query fee: SQL internal."
                })
    return json.dumps({
        "status": 0,
        "msg": "marked fee as canceled successfully."
    })


# Redstone-release
@admin.route("/fee/delete", methods=["POST"])
def fee_remove():
    if not session.get("isLogin"):
        return json.dumps({
            "status": 1,
            "msg": "at judgement isLogin: Not login yet."
        })
    try:
        data = json.loads(request.get_data(as_text=True))
    except:
        return json.dumps({
            "status": 1,
            "msg": "at json parse: JSON parse error."
        })

    feeIds = data.get("feeIds", None)

    for feeId in feeIds:
        selfPrivilege = session.get("privilege", None)
        if selfPrivilege not in ["supervisor", "propertyAdmin", "propertyDeputy"]:
            return json.dumps({
                "status": 1,
                "msg": "at judgement privilege: Insufficient privilege update."
            })

        if selfPrivilege in ["supervisor", "propertyAdmin"]:
            try:
                if len(database.session.query(Fee).filter(and_(Fee.id == feeId, or_(Fee.status == "canceled",
                                                                                    Fee.status == "paid"))).all()) > 0 and selfPrivilege == "propertyAdmin":
                    return json.dumps({
                        "status": 1,
                        "msg": "at check isCanceledOrPaidFee: the fee has been canceled or paid, privilege insufficient."
                    })
                database.session.query(Fee).filter(Fee.id == feeId).delete()
                database.session.commit()
            except:
                return json.dumps({
                    "status": 1,
                    "msg": "at query fee: SQL internal."
                })
        else:
            try:
                if len(database.session.query(Fee).filter(
                        and_(Fee.id == feeId, or_(Fee.status == "canceled", Fee.status == "paid"))).all()) > 0:
                    return json.dumps({
                        "status": 1,
                        "msg": "at check isCanceledOrPaidFee: the fee has been canceled or paid, privilege insufficient."
                    })
                if len(database.session.query(Fee).filter(
                        and_(Fee.id == feeId, session.get("uid") == Fee.superUid)).all()) < 1:
                    return json.dumps({
                        "status": 1,
                        "msg": "at query fee: No log found."
                    })
                database.session.query(Fee).filter(and_(Fee.id == feeId, session.get("uid") == Fee.superUid)).delete()
                database.session.commit()
            except:
                return json.dumps({
                    "status": 1,
                    "msg": "at query fee: SQL internal."
                })
    return json.dumps({
        "status": 0,
        "msg": "delete fee successfully."
    })


# Redstone-release
@admin.route("/life", methods=["GET"])
def life_get():
    if session.get("privilege") not in ["supervisor", "propertyAdmin", "propertyDeputy"]:
        return json.dumps({
            "status": 1,
            "msg": "at judgement privilege: Forbidden."
        })

    start = request.args.get("start", 0, int)
    count = request.args.get("count", 10, int)

    selfUid = session.get("uid", None)

    if session.get("privilege") == "propertyDeputy":
        try:
            total = database.session.query(func.count(WaterElectricity.id)).filter(
                WaterElectricity.superUid == selfUid).scalar()
            rows = database.session.query(WaterElectricity).filter(WaterElectricity.superUid == selfUid).limit(
                count).offset(start).all()
        except:
            return json.dumps({
                "status": 1,
                "msg": "at query users info: SQL internal."
            })
    else:
        try:
            total = database.session.query(func.count(WaterElectricity.id)).scalar()
            rows = database.session.query(WaterElectricity).limit(count).offset(start).all()
        except:
            return json.dumps({
                "status": 1,
                "msg": "at query users info: SQL internal."
            })

    data = [{
        "id": row.id,
        # Carbon Design Svelte DataTable needs an id, reference to https://github.com/carbon-design-system/carbon-components-svelte/issues/596
        "uid": row.uid,
        "water": float(row.waterPrice) * float(row.water),
        "electricity": float(row.electricityPrice) * float(row.electricity),
        "date": str(row.date),
        "status": row.status
    } for row in rows]
    return json.dumps({
        "status": 0,
        "data": data,
        "total": total
    })


# Redstone-release
@admin.route("/life/add", methods=["POST"])
def life_add():
    if not session.get("isLogin"):
        return json.dumps({
            "status": 1,
            "msg": "at judgement isLogin: Not login yet."
        })
    try:
        data = json.loads(request.get_data(as_text=True))
    except:
        return json.dumps({
            "status": 1,
            "msg": "at json parse: JSON parse error."
        })

    water = data.get("water", 0)
    electricity = data.get("electricity", 0)
    waterPrice = data.get("waterPrice", 0)
    electricityPrice = data.get("electricityPrice", 0)
    date = data.get("date", None)
    status = data.get("status", None)
    uid = data.get("uid", None)
    superUid = session.get("uid", None) if session.get("uid") != 0 else data.get("superUid")

    privilege = session.get("privilege", None)
    if privilege not in ["supervisor", "propertyAdmin", "propertyDeputy"] or uid == 0:
        return json.dumps({
            "status": 1,
            "msg": "at judgement privilege: Insufficient privilege update."
        })

    if uid is None or status is None or (water == 0 and waterPrice == 0 and electricity == 0 and electricityPrice == 0):
        return json.dumps({
            "status": 1,
            "msg": "at judgement not null: Insufficient data."
        })

    try:
        database.session.add(WaterElectricity(
            water=water,
            electricity=electricity,
            waterPrice=waterPrice,
            electricityPrice=electricityPrice,
            date=date,
            status=status,
            uid=uid,
            superUid=superUid
        ))
        database.session.commit()
    except:
        return json.dumps({
            "status": 1,
            "msg": "at insertion: SQL internal."
        })

    return json.dumps({
        "status": 0,
        "msg": "WaterElectricity add successfully."
    })


# Redstone-release
@admin.route("/life/<int:lifeId>", methods=["POST"])
def life_id_post(lifeId):
    if not session.get("isLogin"):
        return json.dumps({
            "status": 1,
            "msg": "at judgement isLogin: Not login yet."
        })
    try:
        data = json.loads(request.get_data(as_text=True))
    except:
        return json.dumps({
            "status": 1,
            "msg": "at json parse: JSON parse error."
        })

    if session.get("privilege") == "supervisor" or session.get("privilege") == "propertyAdmin":
        try:
            database.session.query(WaterElectricity).filter(WaterElectricity.id == data["id"]).update({
                WaterElectricity.water: data["water"],
                WaterElectricity.electricity: data["electricity"],
                WaterElectricity.waterPrice: data["waterPrice"],
                WaterElectricity.electricityPrice: data["electricityPrice"],
                WaterElectricity.date: data["date"],
                WaterElectricity.status: data["status"],
                WaterElectricity.uid: data["uid"],
                WaterElectricity.superUid: data["superUid"] if (
                        session.get("privilege") == "supervisor") else session.get("uid")
            })
            database.session.commit()
        except:
            return json.dumps({
                "status": 1,
                "msg": "at query users info: SQL internal."
            })
    elif session.get("privilege") == "propertyDeputy":
        try:
            rows = database.session.query(WaterElectricity.id).filter(
                WaterElectricity.superUid == session.get("uid")).all()
            if len(rows) < 1:
                return json.dumps({
                    "status": 1,
                    "msg": "at judgement privilege: Insufficient privilege update."
                })
            else:
                try:
                    database.session.query(WaterElectricity).filter(WaterElectricity.id == data["id"]).update({
                        WaterElectricity.water: data["water"],
                        WaterElectricity.electricity: data["electricity"],
                        WaterElectricity.waterPrice: data["waterPrice"],
                        WaterElectricity.electricityPrice: data["electricityPrice"],
                        WaterElectricity.date: data["date"],
                        WaterElectricity.uid: data["uid"],
                        WaterElectricity.superUid: session.get("uid")
                    })
                    database.session.commit()
                except:
                    raise
        except:
            return json.dumps({
                "status": 1,
                "msg": "at query water and electricity data: SQL internal."
            })

    return json.dumps({
        "status": 0,
        "msg": "water and electricity data update successfully."
    })


# Redstone-release
@admin.route("/life/<int:lifeId>", methods=["GET"])
def life_id_get(lifeId):
    if session.get("privilege", None) not in ["supervisor", "propertyAdmin", "propertyDeputy"]:
        return json.dumps({
            "status": 1,
            "msg": "at judgement privilege: Forbidden."
        })

    try:
        rows = database.session.query(WaterElectricity).filter(WaterElectricity.id == lifeId).all()
    except:
        return json.dumps({
            "status": 1,
            "msg": "at query fee: SQL internal."
        })

    if len(rows) != 1:
        return json.dumps({
            "status": 1,
            "msg": "at judgement fee log length: length is not 1."
        })

    if session.get("privilege", None) == "propertyDeputy" and rows[0].superUid != session.get("uid", None):
        return json.dumps({
            "status": 1,
            "msg": "at judgement privilege: Forbidden."
        })

    return json.dumps({
        "status": 0,
        "data": rows[0].toDict()
    })


# Redstone-release
@admin.route("/life/pushFee", methods=["POST"])
def life_id_push_fee_post():
    if not session.get("isLogin"):
        return json.dumps({
            "status": 1,
            "msg": "at judgement isLogin: Not login yet."
        })
    try:
        data = json.loads(request.get_data(as_text=True))
    except:
        return json.dumps({
            "status": 1,
            "msg": "at json parse: JSON parse error."
        })

    try:
        rows = database.session.query(WaterElectricity).filter(WaterElectricity.id == data["id"]).all()
        if len(rows) < 1:
            return json.dumps({
                "status": 1,
                "msg": "at judgement logIsExist: Insufficient log applied for."
            })
    except:
        return json.dumps({
            "status": 1,
            "msg": "at query users info: SQL internal."
        })

    if session.get("privilege") == "supervisor" or session.get("privilege") == "propertyAdmin":
        try:
            database.session.query(WaterElectricity).filter(WaterElectricity.id == data["id"]).update({
                WaterElectricity.water: data["water"],
                WaterElectricity.electricity: data["electricity"],
                WaterElectricity.waterPrice: data["waterPrice"],
                WaterElectricity.electricityPrice: data["electricityPrice"],
                WaterElectricity.date: data["date"],
                WaterElectricity.status: "pushed",
                WaterElectricity.uid: data["uid"],
                WaterElectricity.superUid: data["superUid"] if (
                        session.get("privilege") == "supervisor") else session.get("uid")
            })
            database.session.commit()
        except:
            return json.dumps({
                "status": 1,
                "msg": "at query users info: SQL internal."
            })
    elif session.get("privilege") == "propertyDeputy":
        try:
            rows = database.session.query(WaterElectricity.id).filter(
                WaterElectricity.superUid == session.get("uid")).all()
            if len(rows) < 1:
                return json.dumps({
                    "status": 1,
                    "msg": "at judgement privilege: Insufficient privilege update."
                })
            else:
                try:
                    database.session.query(WaterElectricity).filter(WaterElectricity.id == data["id"]).update({
                        WaterElectricity.water: data["water"],
                        WaterElectricity.electricity: data["electricity"],
                        WaterElectricity.waterPrice: data["waterPrice"],
                        WaterElectricity.electricityPrice: data["electricityPrice"],
                        WaterElectricity.date: data["date"],
                        WaterElectricity.status: "pushed",
                        WaterElectricity.uid: data["uid"],
                        WaterElectricity.superUid: session.get("uid")
                    })
                    database.session.commit()
                except:
                    raise
        except:
            return json.dumps({
                "status": 1,
                "msg": "at query fee: SQL internal."
            })

    try:
        database.session.add(Fee(
            type="电费",
            description=f"{data['uid']}的电费",
            amount=float(data["electricity"]) * float(data["electricityPrice"]),
            date=data["date"],
            status="pending",
            uid=data["uid"],
            superUid=data["superUid"] if (session.get("privilege") == "supervisor") else session.get("uid")
        ))
        database.session.commit()

        database.session.add(Fee(
            type="水费",
            description=f"{data['uid']}的水费",
            amount=float(data["water"]) * float(data["waterPrice"]),
            date=data["date"],
            status="pending",
            uid=data["uid"],
            superUid=data["superUid"] if (session.get("privilege") == "supervisor") else session.get("uid")
        ))
        database.session.commit()

    except:
        return json.dumps({
            "status": 1,
            "msg": "at insertion: SQL internal."
        })

    return json.dumps({
        "status": 0,
        "msg": "water and electricity fee pushed successfully."
    })


# Redstone-release
@admin.route("/fix", methods=["GET"])
def fix_get():
    if session.get("privilege") not in ["supervisor", "propertyAdmin", "propertyDeputy"]:
        return json.dumps({
            "status": 1,
            "msg": "at judgement privilege: Forbidden."
        })

    start = request.args.get("start", 0, int)
    count = request.args.get("count", 10, int)

    try:
        total = database.session.query(func.count(Event.id)).scalar()
        rows = database.session.query(Event).limit(count).offset(start).all()
    except:
        return json.dumps({
            "status": 1,
            "msg": "at query users info: SQL internal."
        })

    data = [{
        "id": row.id,
        # Carbon Design Svelte DataTable needs an id, reference to https://github.com/carbon-design-system/carbon-components-svelte/issues/596
        "description": row.description,
        "status": row.status,
        "uid": row.uid
    } for row in rows]
    return json.dumps({
        "status": 0,
        "data": data,
        "total": total
    })


# Redstone-release
@admin.route("/fix/add", methods=["POST"])
def fix_add():
    if not session.get("isLogin"):
        return json.dumps({
            "status": 1,
            "msg": "at judgement isLogin: Not login yet."
        })
    try:
        data = json.loads(request.get_data(as_text=True))
    except:
        return json.dumps({
            "status": 1,
            "msg": "at json parse: JSON parse error."
        })

    description = data.get("description", None)
    status = "pending"
    uid = session.get("uid", None)
    data = data.get("data", None)

    privilege = session.get("privilege", None)
    if privilege not in ["supervisor", "propertyAdmin", "propertyDeputy"]:
        return json.dumps({
            "status": 1,
            "msg": "at judgement privilege: Insufficient privilege update."
        })

    if description is None or status is None or uid is None or data is None:
        return json.dumps({
            "status": 1,
            "msg": "at judgement not null: Insufficient data."
        })

    try:
        database.session.add(Event(
            description=description,
            status=status,
            uid=uid,
            data=data
        ))
        database.session.commit()
    except:
        return json.dumps({
            "status": 1,
            "msg": "at insertion: SQL internal."
        })

    return json.dumps({
        "status": 0,
        "msg": "Event add successfully."
    })


# Redstone-release
@admin.route("/fix/<int:fixId>", methods=["GET"])
def fix_id_get(fixId):
    if session.get("privilege", None) not in ["supervisor", "propertyAdmin", "propertyDeputy"]:
        return json.dumps({
            "status": 1,
            "msg": "at judgement privilege: Forbidden."
        })

    try:
        rows = database.session.query(Event).filter(Event.id == fixId).all()
    except:
        return json.dumps({
            "status": 1,
            "msg": "at query fee: SQL internal."
        })

    if len(rows) != 1:
        return json.dumps({
            "status": 1,
            "msg": "at judgement fee log length: length is not 1."
        })

    return json.dumps({
        "status": 0,
        "data": rows[0].toDict()
    })


# Redstone-release
@admin.route("/fix/<int:fixId>", methods=["POST"])
def fix_id_post(fixId):
    if not session.get("isLogin"):
        return json.dumps({
            "status": 1,
            "msg": "at judgement isLogin: Not login yet."
        })
    try:
        data = json.loads(request.get_data(as_text=True))
    except:
        return json.dumps({
            "status": 1,
            "msg": "at json parse: JSON parse error."
        })

    try:
        database.session.query(Event).filter(Event.id == data["id"]).update({
            Event.description: data["description"],
            Event.status: data["status"],
            Event.uid: data["uid"],
            Event.data: json.dumps(json.loads(data["data"]) + [data["content"]])
        })
        database.session.commit()
    except:
        return json.dumps({
            "status": 1,
            "msg": "at query users info: SQL internal."
        })

    return json.dumps({
        "status": 0,
        "msg": "Event data update successfully."
    })


# Redstone-release
@admin.route("/fix/markAsArchived", methods=["POST"])
def fix_mark_archived():
    if not session.get("isLogin"):
        return json.dumps({
            "status": 1,
            "msg": "at judgement isLogin: Not login yet."
        })
    try:
        data = json.loads(request.get_data(as_text=True))
    except:
        return json.dumps({
            "status": 1,
            "msg": "at json parse: JSON parse error."
        })

    fixIds = data.get("fixIds", None)

    for fixId in fixIds:
        try:
            database.session.query(Event).filter(Event.id == fixId).update({
                Fee.status: "archived"
            })
            database.session.commit()
        except:
            return json.dumps({
                "status": 1,
                "msg": "at query fee: SQL internal."
            })
    return json.dumps({
        "status": 0,
        "msg": "marked fee log as paid successfully."
    })


# Redstone-release
@admin.route("/fix/markAsCanceled", methods=["POST"])
def fix_mark_canceled():
    if not session.get("isLogin"):
        return json.dumps({
            "status": 1,
            "msg": "at judgement isLogin: Not login yet."
        })
    try:
        data = json.loads(request.get_data(as_text=True))
    except:
        return json.dumps({
            "status": 1,
            "msg": "at json parse: JSON parse error."
        })

    fixIds = data.get("fixIds", None)

    for fixId in fixIds:
        try:
            database.session.query(Event).filter(Event.id == fixId).update({
                Fee.status: "canceled"
            })
            database.session.commit()
        except:
            return json.dumps({
                "status": 1,
                "msg": "at query fee: SQL internal."
            })

    return json.dumps({
        "status": 0,
        "msg": "marked fee as canceled successfully."
    })


# Redstone-release
@admin.route("/fix/delete", methods=["POST"])
def fix_remove():
    if not session.get("isLogin"):
        return json.dumps({
            "status": 1,
            "msg": "at judgement isLogin: Not login yet."
        })
    try:
        data = json.loads(request.get_data(as_text=True))
    except:
        return json.dumps({
            "status": 1,
            "msg": "at json parse: JSON parse error."
        })

    fixIds = data.get("fixIds", None)

    for fixId in fixIds:
        selfPrivilege = session.get("privilege", None)
        if selfPrivilege not in ["supervisor"]:
            return json.dumps({
                "status": 1,
                "msg": "at judgement privilege: Insufficient privilege update."
            })
        try:
            database.session.query(Event).filter(Event.id == fixId).delete()
            database.session.commit()
        except:
            return json.dumps({
                "status": 1,
                "msg": "at query fee: SQL internal."
            })
    return json.dumps({
        "status": 0,
        "msg": "delete fee successfully."
    })
