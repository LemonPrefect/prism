import json
import os
from flask import Blueprint, request, session
from sqlalchemy import or_, and_, func
from app import database
from ..models import User, WaterElectricity, Fee, Event

root = Blueprint("root", __name__)


# Redstone-release
@root.route("/", methods=["GET", "POST"])
def default():
    return json.dumps({
        "status": 0,
        "msg": "Use the bedrock to access to the APIs."
    })


# Redstone-release
@root.route("/init", methods=["POST"])
def init():
    if os.path.exists("./init.lock"):
        return json.dumps({
            "status": 1,
            "msg": "lock file is created, initialization has already finished."
        })
    else:
        try:
            database.create_all()
        except:
            return json.dumps({
                "status": 1,
                "msg": "Initialization failed at create database."
            })
        try:
            open("./init.lock", "w").write("lockfile")
        except:
            return json.dumps({
                "status": 1,
                "msg": "Initialization failed at create lock file."
            })
        try:
            database.session.add(User(
                uid=0,
                privilege="supervisor",
                password="admin",
                name="supervisor",
                status=0,
                tid="0",
                email="me@lemonprefect.cn",
                telephone="13066817260",
                room="32-443",
                area="32",
                peopleCount=4,
                parkingLot="0"
            ))
            database.session.commit()

            database.session.query(User).filter(User.uid == 1).update({"uid": 0})
            database.session.commit()

        except:
            return json.dumps({
                "status": 1,
                "msg": "Initialization failed at create supervisor."
            })
        return json.dumps({
            "status": 0,
            "msg": "Initialization successfully."
        })


# Redstone-release
@root.route("/login", methods=["POST"])
def login():
    if session.get("isLogin"):
        return json.dumps({
            "status": 0,
            "msg": "at judgement isLogin: Already login."
        })
    try:
        data = json.loads(request.get_data(as_text=True))
    except:
        return json.dumps({
            "status": 1,
            "msg": "at json parse: JSON parse error."
        })
    password = data.get("password", None)
    email = data.get("email", None)
    telephone = data.get("telephone", None)

    if password is None or (email is None and telephone is None):
        return json.dumps({
            "status": 1,
            "msg": "at judgement no null: Insufficient evidence."
        })

    row = database.session.query(User).filter(or_(User.email == email, User.telephone == telephone),
                                              User.password == password).all()
    if len(row) != 1:
        return json.dumps({
            "status": 1,
            "msg": "at login: Evidence invalid."
        })

    if row[0].status == 1:
        return json.dumps({
            "status": 1,
            "msg": "at login: Account has been disabled."
        })

    session["isLogin"] = True
    session["evidence"] = email if email is not None else telephone
    session["tid"] = row[0].tid
    session["uid"] = row[0].uid
    session["name"] = row[0].name
    session["privilege"] = row[0].privilege

    return json.dumps({
        "status": 0,
        "msg": "Login successfully."
    })


# Redstone-release
@root.route("/register", methods=["POST"])
def register():
    try:
        data = json.loads(request.get_data(as_text=True))
    except:
        return json.dumps({
            "status": 1,
            "msg": "at json parse: JSON parse error."
        })
    password = data.get("password", None)
    name = data.get("name", None)
    tid = data.get("tid", None)
    email = data.get("email", None)
    telephone = data.get("telephone", None)
    area = data.get("area", None)
    room = data.get("room", None)
    peopleCount = data.get("peopleCount", None)
    parkingLot = data.get("parkingLot", None)

    if password is None or (
            email is None and telephone is None) or area is None or room is None or name is None or tid is None:
        return json.dumps({
            "status": 1,
            "msg": "at judgement not null: Insufficient data."
        })

    if len(database.session.query(User).filter(or_(
            and_(User.telephone == telephone, telephone is not None),
            and_(User.email == email, email is not None),
            User.tid == tid
    )).all()) != 0:
        return json.dumps({
            "status": 1,
            "msg": "at judgement duplication: Duplicated data."
        })

    try:
        database.session.add(User(
            privilege="owner",
            password=password,
            name=name,
            status=1,
            tid=tid,
            email=email,
            telephone=telephone,
            area=area,
            room=room,
            peopleCount=peopleCount,
            parkingLot=parkingLot
        ))
        database.session.commit()
    except:
        return json.dumps({
            "status": 1,
            "msg": "at insertion: SQL internal."
        })

    return json.dumps({
        "status": 0,
        "msg": "Register successfully."
    })


# Redstone-release
@root.route("/logout", methods=["GET", "POST"])
def logout():
    session.clear()
    return json.dumps({
        "status": 0,
        "msg": "Logout successfully."
    })


# Redstone-release
@root.route("/privilege", methods=["GET"])
def privilege():
    if not session.get("isLogin"):
        return json.dumps({
            "status": 1,
            "msg": "at judgement isLogin: Not login yet."
        })
    return json.dumps({
        "status": 0,
        "privilege": session.get("privilege")
    })


# Redstone-release
@root.route("/user", methods=["GET"])
def self_user():
    if session.get("isLogin") is not True:
        return json.dumps({
            "status": 1,
            "msg": "at judgement isLogin: Insufficient evidence."
        })
    rows = database.session.query(User).filter(
        or_(User.email == session.get("evidence"), User.telephone == session.get("evidence")),
        User.tid == session.get("tid")
    ).all()
    if len(rows) != 1:
        return json.dumps({
            "status": 1,
            "msg": "at judgement userinfo length: length is not 1."
        })

    return json.dumps({
        "status": 0,
        "data": rows[0].toDict()
    })


# Redstone-release
@root.route("/user", methods=["POST"])
def update_user():
    if session.get("isLogin") is not True:
        return json.dumps({
            "status": 1,
            "msg": "at judgement isLogin: Insufficient evidence."
        })
    try:
        data = json.loads(request.get_data(as_text=True))
    except:
        return json.dumps({
            "status": 1,
            "msg": "at json parse: JSON parse error."
        })

    uid = data.get("uid", None)
    password = data.get("password", None)
    peopleCount = data.get("peopleCount", None)
    parkingLot = data.get("parkingLot", None)

    try:
        database.session.query(User).filter(User.uid == uid).update({
            "password": password,
            "peopleCount": peopleCount,
            "parkingLot": parkingLot
        })
        database.session.commit()
    except:
        return json.dumps({
            "status": 1,
            "msg": "at update data: SQL internal."
        })

    return json.dumps({
        "status": 0,
        "msg": "Userinfo update successfully."
    })


# Redstone-release
@root.route("/life", methods=["GET"])
def life_get():
    if session.get("isLogin") is not True:
        return json.dumps({
            "status": 1,
            "msg": "at judgement isLogin: Insufficient evidence."
        })

    start = request.args.get("start", 0, int)
    count = request.args.get("count", 10, int)

    selfUid = session.get("uid", None)

    try:
        total = database.session.query(func.count(WaterElectricity.id)).scalar()
        rows = database.session.query(WaterElectricity).filter(WaterElectricity.uid == selfUid).limit(count).offset(
            start).all()
    except:
        return json.dumps({
            "status": 1,
            "msg": "at query users info: SQL internal."
        })

    data = [{
        "id": row.id,
        # Carbon Design Svelte DataTable needs an id, reference to
        # https://github.com/carbon-design-system/carbon-components-svelte/issues/596
        "water": row.water,
        "waterPrice": row.waterPrice,
        "electricity": row.electricity,
        "electricityPrice": row.electricityPrice,
        "date": str(row.date),
    } for row in rows]
    return json.dumps({
        "status": 0,
        "data": data,
        "total": total
    })


# Redstone-release
@root.route("/life/<int:lifeId>", methods=["GET"])
def life_id_get(lifeId):
    if session.get("isLogin") is not True:
        return json.dumps({
            "status": 1,
            "msg": "at judgement isLogin: Insufficient evidence."
        })

    try:
        rows = database.session.query(WaterElectricity).filter(
            and_(WaterElectricity.id == lifeId, WaterElectricity.uid == session.get("uid"))).all()
    except:
        return json.dumps({
            "status": 1,
            "msg": "at query fee: SQL internal."
        })

    if len(rows) != 1:
        return json.dumps({
            "status": 1,
            "msg": "at judgement fee log length: length is not 1."
        })

    return json.dumps({
        "status": 0,
        "data": rows[0].toDict()
    })


# Redstone-release
@root.route("/fee", methods=["GET"])
def fee_get():
    if session.get("isLogin") is not True:
        return json.dumps({
            "status": 1,
            "msg": "at judgement isLogin: Insufficient evidence."
        })

    start = request.args.get("start", 0, int)
    count = request.args.get("count", 10, int)

    try:
        total = database.session.query(func.count(Fee.id)).scalar()
        rows = database.session.query(Fee).filter(Fee.uid == session.get("uid", None)).limit(count).offset(start).all()
    except:
        return json.dumps({
            "status": 1,
            "msg": "at query users info: SQL internal."
        })

    data = [{
        "id": row.id,
        # Carbon Design Svelte DataTable needs an id, reference to https://github.com/carbon-design-system/carbon-components-svelte/issues/596
        "type": row.type,
        "amount": row.amount,
        "uid": row.uid,
        "date": str(row.date),
        "status": row.status
    } for row in rows]

    return json.dumps({
        "status": 0,
        "data": data,
        "total": total
    })


# Redstone-release
@root.route("/fee/<int:feeId>", methods=["GET"])
def fee_id_get(feeId):
    if session.get("isLogin") is not True:
        return json.dumps({
            "status": 1,
            "msg": "at judgement isLogin: Insufficient evidence."
        })

    try:
        rows = database.session.query(Fee).filter(and_(Fee.id == feeId, Fee.uid == session.get("uid"))).all()
    except:
        return json.dumps({
            "status": 1,
            "msg": "at query fee: SQL internal."
        })

    if len(rows) != 1:
        return json.dumps({
            "status": 1,
            "msg": "at judgement fee log length: length is not 1."
        })

    return json.dumps({
        "status": 0,
        "data": rows[0].toDict()
    })


# Redstone-release
@root.route("/fix", methods=["GET"])
def fix_get():
    if session.get("isLogin") is not True:
        return json.dumps({
            "status": 1,
            "msg": "at judgement isLogin: Insufficient evidence."
        })

    start = request.args.get("start", 0, int)
    count = request.args.get("count", 10, int)

    selfUid = session.get("uid", None)

    try:
        total = database.session.query(func.count(Event.id)).scalar()
        rows = database.session.query(Event).filter(Event.uid == selfUid).limit(count).offset(start).all()
    except:
        return json.dumps({
            "status": 1,
            "msg": "at query users info: SQL internal."
        })

    data = [{
        "id": row.id,
        # Carbon Design Svelte DataTable needs an id, reference to
        # https://github.com/carbon-design-system/carbon-components-svelte/issues/596
        "description": row.description,
        "status": row.status,
        "uid": row.uid
    } for row in rows]
    return json.dumps({
        "status": 0,
        "data": data,
        "total": total
    })


# Redstone-release
@root.route("/fix/add", methods=["POST"])
def fix_add():
    if not session.get("isLogin"):
        return json.dumps({
            "status": 1,
            "msg": "at judgement isLogin: Not login yet."
        })
    try:
        data = json.loads(request.get_data(as_text=True))
    except:
        return json.dumps({
            "status": 1,
            "msg": "at json parse: JSON parse error."
        })

    description = data.get("description", None)
    status = "pending"
    uid = session.get("uid", None)
    data = data.get("data", None)

    if description is None or status is None or uid is None or data is None:
        return json.dumps({
            "status": 1,
            "msg": "at judgement not null: Insufficient data."
        })

    try:
        database.session.add(Event(
            description=description,
            status=status,
            uid=uid,
            data=data
        ))
        database.session.commit()
    except:
        return json.dumps({
            "status": 1,
            "msg": "at insertion: SQL internal."
        })

    return json.dumps({
        "status": 0,
        "msg": "Fix event add successfully."
    })


# Redstone-release
@root.route("/fix/markAsArchived", methods=["POST"])
def fix_mark_archived():
    if not session.get("isLogin"):
        return json.dumps({
            "status": 1,
            "msg": "at judgement isLogin: Not login yet."
        })
    try:
        data = json.loads(request.get_data(as_text=True))
    except:
        return json.dumps({
            "status": 1,
            "msg": "at json parse: JSON parse error."
        })

    fixIds = data.get("fixIds", None)

    for fixId in fixIds:
        try:
            database.session.query(Event).filter(and_(Event.id == fixId, Event.uid == session.get("uid"))).update({
                Fee.status: "archived"
            })
            database.session.commit()
        except:
            return json.dumps({
                "status": 1,
                "msg": "at query fee: SQL internal."
            })
    return json.dumps({
        "status": 0,
        "msg": "marked fee log as paid successfully(if there exists a log matched the condition for your uid)."
    })


# Redstone-release
@root.route("/fix/markAsCanceled", methods=["POST"])
def fix_mark_canceled():
    if not session.get("isLogin"):
        return json.dumps({
            "status": 1,
            "msg": "at judgement isLogin: Not login yet."
        })
    try:
        data = json.loads(request.get_data(as_text=True))
    except:
        return json.dumps({
            "status": 1,
            "msg": "at json parse: JSON parse error."
        })

    fixIds = data.get("fixIds", None)

    for fixId in fixIds:
        try:
            database.session.query(Event).filter(and_(Event.id == fixId, Event.uid == session.get("uid"))).update({
                Fee.status: "canceled"
            })
            database.session.commit()
        except:
            return json.dumps({
                "status": 1,
                "msg": "at query fee: SQL internal."
            })

    return json.dumps({
        "status": 0,
        "msg": "marked fee as canceled successfully(if there exists a log matched the condition for your uid)."
    })


# Redstone-release
@root.route("/fix/<int:fixId>", methods=["GET"])
def fix_id_get(fixId):
    if session.get("isLogin") is not True:
        return json.dumps({
            "status": 1,
            "msg": "at judgement isLogin: Insufficient evidence."
        })

    try:
        rows = database.session.query(Event).filter(and_(Event.id == fixId, Event.uid == session.get("uid"))).all()
    except:
        return json.dumps({
            "status": 1,
            "msg": "at query fee: SQL internal."
        })

    if len(rows) != 1:
        return json.dumps({
            "status": 1,
            "msg": "at judgement fee log length: length is not 1."
        })

    return json.dumps({
        "status": 0,
        "data": rows[0].toDict()
    })


# Redstone-release
@root.route("/fix/<int:fixId>", methods=["POST"])
def fix_id_post(fixId):
    if not session.get("isLogin"):
        return json.dumps({
            "status": 1,
            "msg": "at judgement isLogin: Not login yet."
        })
    try:
        data = json.loads(request.get_data(as_text=True))
    except:
        return json.dumps({
            "status": 1,
            "msg": "at json parse: JSON parse error."
        })

    try:
        database.session.query(Event).filter(and_(Event.id == data["id"], Event.uid == session.get("uid"))).update({
            Event.description: data["description"],
            Event.status: data["status"],
            Event.uid: data["uid"],
            Event.data: json.dumps(json.loads(data["data"]) + [data["content"]])
        })
        database.session.commit()
    except:
        return json.dumps({
            "status": 1,
            "msg": "at query users info: SQL internal."
        })

    return json.dumps({
        "status": 0,
        "msg": "fix event data update successfully(if there exists an log matched the condition for your uid)."
    })
