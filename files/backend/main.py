from app import app

# app.run("0.0.0.0", 80, debug=True)

from gevent import pywsgi
server = pywsgi.WSGIServer(('0.0.0.0', 80), app)
server.serve_forever()
