/** @type {import('@sveltejs/kit').Config} */
import adapter from '@sveltejs/adapter-static';
const config = {
	kit: {
		// hydrate the <div id="svelte"> element in src/app.html
		adapter: adapter(),
		prerender: {
            crawl: true,
            enabled: true,
            onError: 'fail'
        },
		target: '#svelte',
	}
};

export default config;